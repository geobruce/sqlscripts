-- Schrijf een script, 0527__Oefening.sql, dat alle auteurs selecteert waarvan de familienaam begint met 'B', maar niet met iets anders, zonder gebruik te maken van LIKE.
USE ModernWays;
Select * FROM boeken WHERE (Familienaam >= "B") AND (Familienaam < "C");