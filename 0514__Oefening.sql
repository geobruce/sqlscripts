-- sorteren
-- Schrijf een script, 0514__Oefening.sql, dat (alleen!) de titels van liedjes toont, gaande van het oudste nummer tot het recentste nummer.
USE ModernWays;
SELECT Liedjes.Titel FROM Liedjes ORDER BY Liedjes.Jaar;