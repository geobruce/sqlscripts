-- Toon met script 0532__Oefening.sql een overzicht van alle muzieknummers,
-- geordend volgens releasejaar (van nieuw naar oud), artiest (van A tot Z) en lengte van de titel (van lang naar kort). 
-- Dit laatste kan je bereiken met de LENGTH-functie, die vergelijkbaar is met CONCAT en SUBSTRING maar slechts één invoer heeft.
USE ModernWays;
SELECT * FROM Liedjes
ORDER BY Jaar DESC, Artiest, LENGTH(Titel) DESC;