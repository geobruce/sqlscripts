-- gedetailleerd sorteren
-- Schrijf een script, 0515__Oefening.sql, dat alle info over huisdieren toont, maar de huisdieren alfabetisch ordent volgens naam en, indien de naam dezelfde is, alfabetisch sorteert volgens soort.
USE ModernWays;
SELECT * FROM huisdieren ORDER BY Naam, Soort;