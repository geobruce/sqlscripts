-- Oefening 3
-- Schrijf een script, 0525__Oefening.sql, dat de zin "X is de naam van een hond" 
-- produceert voor elke hond in de database. De resultaten van je opdracht worden weergegeven
--  in een tabel met één kolom, met als hoofding "Bewering".

USE ModernWays;

SELECT concat(Naam, " is de naam van een hond") FROM Huisdieren WHERE Soort = "Hond";