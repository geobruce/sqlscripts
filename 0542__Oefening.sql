USE ModernWays;
SELECT Artiest, SUM(Aantalbeluisteringen) AS "aantal beluisteringen"
FROM Liedjes
GROUP BY Artiest
HAVING SUM(Aantalbeluisteringen) > 100;