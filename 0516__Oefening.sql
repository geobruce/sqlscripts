-- Schrijf een script, 0516__Oefening.sql, dat per dier een tabel met één kolom, een omschrijving, toont. Deze heeft volgende vorm, inclusief de juiste hoofding:
-- Omschrijving
-- Misty de hond
-- Ming de hond
-- Bientje de kat
-- Flip de papegaai
-- Berto de papegaai
-- Ming de kat
-- Suerta de hond
-- Фёдор de hond

USE ModernWays;
SELECT concat(Naam, 'de', Soort) AS 'Omschrijving' FROM huisdieren;