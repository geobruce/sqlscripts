-- Schrijf een script, 0528__Oefening.sql, dat alle boeken selecteert die ten laatste zijn uitgekomen in 1999, zonder gebruik te maken van LIKE.
USE ModernWays;
SELECT * FROM Boeken WHERE Verschijningsjaar < 2000;