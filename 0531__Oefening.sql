-- Toon met een script, 0531__Oefening.sql, alle boeken zonder commentaar waarvan de auteur een achternaam heeft die met een letter vanaf "D" begint.
USE ModernWays;
SELECT * FROM Boeken WHERE (Commentaar IS NULL) AND Familienaam >= 'D';