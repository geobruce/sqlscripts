-- informatie toevoegen
-- Schrijf een script, 0518__Oefening.sql, dat volgende zaken doet:
--     een niet-verplichte kolom Geluid toevoegen aan de tabel met huisdieren, die tekst (tot 20 karakters uit het Engels) kan bevatten
--         tip: dit is een DDL-statement, dus kijk terug naar de DDL indien nodig
--     het geluid van alle honden aanpast naar "WAF!"
--     het geluid van alle katten aanpast naar "miauwww..."
USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN Geluid VARCHAR(20); -- CHAR SET utf8mb4;

SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Geluid = 'WAF!' WHERE huisdieren.Soort = 'hond';
UPDATE huisdieren SET Geluid = 'miauwww...' WHERE huisdieren.Soort = 'kat';
SET SQL_SAFE_UPDATES = 1;