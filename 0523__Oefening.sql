USE ModernWays;
SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Leeftijd = 9
WHERE 
(huisdieren.Soort = "Hond" AND huisdieren.Baasje = "Christiane")
OR 
(huisdieren.Soort = "Kat" AND huisdieren.Baasje = "Bert");
SET SQL_SAFE_UPDATES = 1;