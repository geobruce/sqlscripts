-- Schrijf een script, 0519__Oefening.sql, dat volgende zaken doet:
-- de niet-verplichte kolom Genre terug toevoegt aan de tabel met nummers (tot 20 karakters uit het Engels)
-- met maximum één UPDATE-operatie alle nummers van Led Zeppelin en Van Halen aanduidt als Hard Rock
USE ModernWays;
ALTER TABLE liedjes ADD COLUMN Genre VARCHAR(20);
SET SQL_SAFE_UPDATES = 0;
UPDATE liedjes SET liedjes.Genre = 'Hard Rock' WHERE (liedjes.Artiest = 'Led Zeppelin') OR (liedjes.Artiest = 'Van Halen');
SET SQL_SAFE_UPDATES = 1;
