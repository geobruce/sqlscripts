-- Familienaam  Titel Verschijningsjaar Categorie
-- ? Beowulf 0975 Mythologie
-- Ovidius Metamorfosen 8 Mythologie

USE ModernWays;
INSERT INTO Boeken (Familienaam, Titel, Verschijningsjaar, Categorie)
VALUES
('?', 'Beowulf', 0975, 'Mythologie'),
('Ovidius', 'Metamorfosen', '8', 'Mythologie');

SELECT * FROM boeken WHERE Verschijningsjaar < 0975;